/**
* Esucha los clicks en los botones y envía el mensaje apropiado
* al script de contenido de la página.
*/
function listenForClicks() {
document.addEventListener("click", (e) => {

function onError(error) {
    console.error(`Error: ${error}`);
}

function entryPig(tabs){
    console.log('Envio desde entryPig');
    browser.tabs.sendMessage(
        tabs[0].id,
        {pig: e.target.textContent}
      ).then(response => {
        console.log(response);
      }).catch(onError);
}
function about(tabs){
    let element = document.getElementById('about');
    if( element.classList.contains('hidden') ) {
        element.classList.remove("hidden");
    } else {
        element.classList.add("hidden");
    }
}

/**
* Imprime el error en consola.
*/
function reportError(error) {
    console.error(`Could not beastify: ${error}`);
}

/**
* Toma la pestaña activa y
* llama a "entryPig()" o "config()" según corresponda.
*/
    if (e.target.classList.contains("option")) {
        browser.tabs.query({active: true, currentWindow: true})
            .then(entryPig)
            .catch(reportError);
    }
    if (e.target.classList.contains("about")) {
        browser.tabs.query({active: true, currentWindow: true})
            .then(about)
            .catch(reportError);
    }
});
}

/**
* Si hubo algún error al ejecutar el script,
* Despliega un popup con el mensaje de error y oculta la UI normal.
*/
function reportExecuteScriptError(error) {
    document.querySelector("#popup-content").classList.add("hidden");
    document.querySelector("#error-content").classList.remove("hidden");
    console.error(`Failed to execute beastify content script: ${error.message}`);
}

/**
* Cuando se carga la ventana emergente, inyecta el script de contenido en la pestaña activa,
* y agrega un manejador de eventos.
* Si no es posible inyectar el script, se ocupa de manejar el error.
*/
browser.tabs.executeScript({file: "/content_scripts/dataentrypig.js"})
.then(listenForClicks)
.catch(reportExecuteScriptError);