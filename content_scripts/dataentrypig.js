(function() {
    /**
     * Atiende mensajes del script de segundo plano.
    */
   
   browser.runtime.onMessage.addListener(request => {
        if(request.pig == "Fill") {
            completarForm();
            res = {response : "Formulario completo"};
        }
        if(request.pig == "About") {
            document.getElementById('about').classList.remove("hidden");
            res = {response : "About done"};
        }
        return Promise.resolve(res);
    });

    function completarForm() {
        console.log('Estoy completando el formulario');
        //Creamos mensaje de success
        let msg = document.createElement("DIV");
        msg.innerText = "Ya terminé oink oink 🐽";
        msg.className += 'data_entry_pig_msg';
        msg.id = "msg_pig";
        document.body.appendChild(msg);
        
        let countFilesError = 0;

        //Obtenemos el primer formulario
        for (let a = 0; a < document.forms.length; a++) {
            let Form = document.forms[a];
            for(I = 0; I < Form.length; I++) {
                //Obtenemos name y completamos de acuerdo al tag
                var Name = Form[I].getAttribute('name');
                if( Form[I].tagName == 'input' || Form[I].tagName == 'INPUT') {
                    if (Form[I].type == "text" && Form[I].getAttribute('name')) {
                        document.querySelector('input[name="'+Name+'"]').value = 'Testing';
                    }
                    if (Form[I].type == "email" && Form[I].getAttribute('name')) {
                        document.querySelector('input[name="'+Name+'"]').value = 'borjas@gmail.com';
                    }
                    else if (Form[I].type == "checkbox") {
                        const checkboxes = document.querySelectorAll(`input[name="${Name}"]`);
                        checkboxes.forEach((checkbox, index) => {
                            if (index == 1) {
                                checkbox.checked = true;
                            }
                        });
                    }
                    else if (Form[I].type == "radio") {
                        const radios = document.querySelectorAll(`input[name="${Name}"]`);
                        radios.forEach((radio) => {
                            radio.checked = true;
                        });
                    }
                    else if (Form[I].type == "file") {
                        console.log("FilesError: "+countFilesError);
                        countFilesError == 0  ? msg.innerText += "\n No pude subir los archivos por tu seguridad oink oink 🐽🐽" : null;
                        countFilesError++;
                    } 
                } else if (Form[I].tagName == 'select' || Form[I].tagName == 'SELECT') {
                    document.querySelector('select[name="'+Name+'"]').getElementsByTagName('option')[1].selected = 'selected';
                }
                else if (Form[I].tagName == 'textarea' || Form[I].tagName == 'TEXTAREA') {
                    document.querySelector('textarea[name="'+Name+'"]').innerHTML = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sapien velit, aliquet eget commodo nec, auctor a sapien.';
                }
            }
            
        }
        msg.style.display = 'block';
        setTimeout(function(){	msg.style.display = 'none'; }, 3000 );
    }

    let styles = `
    .float_data_entry_pig{
        position:fixed;
        width:140px;
        height:60px;
        bottom:40px;
        right:40px;
        border-radius:50px;
        text-align:center;
        box-shadow: 2px 2px 3px #999;
        z-index: 1000;
    }
    .data_entry_pig_msg {
        position: fixed;
        top: 60px;
        left: 20px;
        padding: 20px;
        background-color: #faebd745;
        z-index: 9999;
        display:none;
    }
    `;

    //Agrega estilos
    var styleSheet = document.createElement("style");
    styleSheet.type = "text/css";
    styleSheet.innerText = styles;
    document.head.appendChild(styleSheet);


})();